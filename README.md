Example package to recreate some of the features of ROS1 into a ROS2 package.
For now, the features are : 
  * C++ and Python files in the same package,
  * Allows nodes in a package to access custom services/messages located in the same package.

This might not always be a good idea to try to do so, but it is still worth noticing how you can do it - and if it is worth it.

# Example

## For building Python files and Cpp files into a same package

Inspired by https://github.com/youliangtan/MySandBox/tree/master/ros2_payload.

To make it work, you'll have to use a CMakeLists (and ament_cmake instead of ament_python).

WARNING : Python files won't use anymore a setup.py file to be installed. This is notably an issue to execute the test files.

- The folder containing the .py files should have a __init__.py file. It should also be given by the following line in the CMakeLists.txt : 

~~~
ament_python_install_package(<folder_name>/)
~~~

You also need to add the following lines (just before the ament_package() line) : 

~~~
install(PROGRAMS
  <folder_name>/<py_file_1>.py
  ...
  <folder_name>/<py_file_n>.py
  DESTINATION  lib/${PROJECT_NAME}
)
~~~

- Please BE SURE that the .py files can be considered as executables!

## For adding a custom Service / Message to a node located inside the same package as those objects

After completing CMakeLists.txt and package.xml according to https://docs.ros.org/en/foxy/Tutorials/Beginner-Client-Libraries/Custom-ROS2-Interfaces.html, you also need to add into CMakeLists.txt : 

~~~
find_package(rmw_implementation_cmake REQUIRED)
~~~

And then, after all your executable are created, you need to add the following lines into CMakeLists.txt : 

~~~
get_available_rmw_implementations(rmw_implementations2)
foreach(rmw_implementation ${rmw_implementations2})
  find_package("${rmw_implementation}" REQUIRED)
  get_rmw_typesupport(typesupport_impls "${rmw_implementation}" LANGUAGE "cpp")
  foreach(typesupport_impl ${typesupport_impls})
    rosidl_target_interfaces(<executable_name_1>
      ${PROJECT_NAME} ${typesupport_impl}
    )
    ....
    rosidl_target_interfaces(<executable_name_n>
      ${PROJECT_NAME} ${typesupport_impl}
    )    
  endforeach()
endforeach()
~~~

For .py files, it seems that you do not need to do anything particular to access those.

NB : from the structure of https://github.com/ros2/example_interfaces/tree/foxy and the tutorial given by https://docs.ros.org/en/foxy/Tutorials/Beginner-Client-Libraries/Writing-A-Simple-Cpp-Service-And-Client.html, it might now be more interesting to simply create one package with all of the custom Msg/Srv, and to call the package everytime.

## For reorganizing your custom Services and Messages in different folders

You just need to add the path to those files into CMakeLists.txt, as usual : 

~~~
rosidl_generate_interfaces(${PROJECT_NAME}
  "srv/<path_to_service_1>.srv"
  "srv/<path_to_service_2>.srv"
)
~~~

Please note that ROS2 do not care about this path to access your custom Messages / Services into your Cpp or Python files. 

Ex : For those two possible paths of your .srv file : 

~~~
srv/String.svr
srv/SubFolder1/.../SubFolderN/String.srv
~~~

The way to access the String.srv into Cpp will always be : 

~~~
#include "<package_name>/srv/string.hpp"
~~~

And in Python : 

~~~
from <package_name>.srv import String
~~~

## For hiding the output of a node in termnial, in launchfile

Obtained from https://design.ros2.org/articles/ros_command_line_arguments.html

You need to do the following modification to not display the output of the logger for each of the concerned node : 

~~~
arguments=['--ros-args', "--disable-stdout-logs"]
~~~

You can also do (better choice) : 

~~~
 arguments=['--ros-args', "--log-level","FATAL|ERROR"]            
~~~

Please also note that the --ros-args argument needs to be used for adding some properties to a node through the commandline "ros2 run".

Ex : for changing the namespace of a node : 

~~~
ros2 run ros2_subscriber_publisher global_client.py --ros-args -r __ns:=/py
~~~

NB : According to https://docs.ros.org/en/foxy/Tutorials/Beginner-Client-Libraries/Using-Parameters-In-A-Class-CPP.html, you might be able to set up a printing into the terminal by adding the following lines : 

~~~
output="screen",
emulate_tty=True,
~~~

However, changing those lines (output="log", emulate_tty=False) do not desactivate the printing into the terminal.


# How to build 

For rosdep : 

~~~
rosdep install -i --from-path src --rosdistro foxy -y
~~~

For building (with symlink) :

~~~
colcon build --symlink-install
~~~

# How to call

You can use the classic way : 

~~~
ros2 run ros2_subscriber_publisher hello_cpp
ros2 run ros2_subscriber_publisher hello_py.py
~~~

