# #!/usr/bin/env python3
# from launch import LaunchDescription
# from launch_ros.actions import Node

# import launch.actions

# from launch.actions import IncludeLaunchDescription
# from launch.actions import LogInfo
# from launch.launch_description_sources import PythonLaunchDescriptionSource
# from launch.substitutions import ThisLaunchFileDir

# def generate_launch_description():
#     return LaunchDescription([
#         launch.actions.DeclareLaunchArgument(
#             'test',
#             default_value='different_default_value',
#             description='test arg that overlaps arg in included file',
#             ),
#         LogInfo(msg=[
#             'Including launch file located at: ', ThisLaunchFileDir(), '/subscriber_launch.py'
#         ]),
#         IncludeLaunchDescription(
#             PythonLaunchDescriptionSource([ThisLaunchFileDir(), '/subscriber_launch.py']),
#             launch_arguments={'node_name': 'bar'}.items(),
#         ),
#     ])
