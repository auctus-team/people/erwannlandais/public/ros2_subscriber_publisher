#!/usr/bin/env python3
from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='ros2_subscriber_publisher',
            namespace='cpp',
            executable='talker_cpp',
            name='talker',
            #arguments=['--ros-args', "--disable-stdout-logs"]
            arguments=['--ros-args', "--log-level","ERROR"],
            parameters=[
                {'string_message':'Startup_cpp'}
            ]                         
        ),
        Node(
            package='ros2_subscriber_publisher',
            namespace='cpp',
            executable='listener_cpp',
            name='listener'
        ),        
        Node(
            package='ros2_subscriber_publisher',
            namespace='py',
            executable='global_publisher.py',
            name='talker',
            arguments=['--ros-args', "--log-level","ERROR"],
            parameters=[
                {'string_message':'Startup_py'}
            ]  
        ),
        Node(
            package='ros2_subscriber_publisher',
            namespace='py',
            executable='global_subscriber.py',
            name='listener'
        )       
    ])
