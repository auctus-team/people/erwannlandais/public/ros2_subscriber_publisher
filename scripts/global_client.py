#!/usr/bin/env python3

import sys

import rclpy
from rclpy.node import Node
from ros2_subscriber_publisher.srv import String as String_srv


class GlobalClientAsyncPy(Node):

    def __init__(self):
        super().__init__('global_client_async_py')
        self.cli = self.create_client(String_srv, 'string_srv_py')
        while not self.cli.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('service not available, waiting again...')
        self.req = String_srv.Request()

    def send_request(self):
        self.req.data = "Py client working!"
        self.future = self.cli.call_async(self.req)


def main(args=None):
    rclpy.init(args=args)

    global_client = GlobalClientAsyncPy()
    global_client.send_request()

    while rclpy.ok():
        rclpy.spin_once(global_client)
        if global_client.future.done():
            try:
                response = global_client.future.result()
            except Exception as e:
                global_client.get_logger().info(
                    'Service call failed %r' % (e,))
            else:
                global_client.get_logger().info(
                    'Message returned : %s' %
                    (response.message))
            break

    global_client.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()