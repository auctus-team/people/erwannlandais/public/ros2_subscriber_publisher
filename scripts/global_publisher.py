#!/usr/bin/env python3
import rclpy
from rclpy.node import Node

from std_msgs.msg import String as String_msg
from ros2_subscriber_publisher.srv import String as String_srv
from ros2_subscriber_publisher.msg import IntFloat

class GlobalPublisherPy(Node):

    def __init__(self):
        super().__init__('global_publisher_py')
        self.publisher_ = self.create_publisher(String_msg, 'topic', 10)

        self.param_string_timer = self.create_timer(1.0, self.string_param_respond)
        self.declare_parameter('string_message', "Hello World")

        self.message_chosen = ""
        self.startUp = False

        timer_period = 0.5  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback_string)

        self.srv_string = self.create_service(String_srv,"string_srv_py", self.server_string)


        self.i = 0


    def string_param_respond(self):
        my_param = self.get_parameter('string_message').get_parameter_value().string_value

        if (my_param != self.message_chosen):
            self.message_chosen = my_param
            self.i = 0
            self.startUp= True

    def timer_callback_string(self):
        if self.startUp:
            msg = String_msg()
            msg.data = self.message_chosen +': %d' % self.i
            self.publisher_.publish(msg)
            self.get_logger().info('Publishing: "%s"' % msg.data)
            self.i += 1

    def server_string(self, request, response):
        answer = ""
        if (self.message_chosen != request.data):
            self.message_chosen = request.data
            self.i = 0
            answer = "Got new data!"
        else: 
            a = 1
        self.get_logger().info(answer)
        response.message = answer
        response.success = True
        return(response)


def main(args=None):
    rclpy.init(args=args)

    global_publisher_py = GlobalPublisherPy()

    rclpy.spin(global_publisher_py)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    global_publisher_py.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
