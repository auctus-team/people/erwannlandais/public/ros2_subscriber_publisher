#!/usr/bin/env python3
import rclpy
from rclpy.node import Node

from std_msgs.msg import String


class GlobalSubscriberPy(Node):

    def __init__(self):
        super().__init__('global_subscriber_py')
        self.subscription = self.create_subscription(
            String,
            'topic',
            self.listener_callback_string,
            10)
        self.subscription  # prevent unused variable warning

    def listener_callback_string(self, msg):
        self.get_logger().info('I heard: "%s"' % msg.data)


def main(args=None):
    rclpy.init(args=args)

    global_subscriber_py = GlobalSubscriberPy()

    rclpy.spin(global_subscriber_py)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    global_subscriber_py.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()