#include <chrono>
#include <functional>
#include <memory>
#include <string>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "ros2_subscriber_publisher/msg/int_float.hpp"
#include "ros2_subscriber_publisher/srv/string.hpp"

using namespace std::chrono_literals;

using std::placeholders::_1;
using std::placeholders::_2;

/* This example creates a subclass of Node and uses std::bind() to register a
* member function as a callback from the timer. */

class GlobalPublisherCpp : public rclcpp::Node
{
  public:
    GlobalPublisherCpp()
    : Node("global_publisher_cpp"), count_(0)
    {
      startUp = false;
      this->declare_parameter<std::string>("string_message", "Hello, world!");
      param_string_timer_ = this->create_wall_timer(
      1000ms, std::bind(&GlobalPublisherCpp::string_param_respond, this));      
      message_chosen = "";
      string_publisher_ = this->create_publisher<std_msgs::msg::String>("topic", 10);
      string_timer_ = this->create_wall_timer(
      500ms, std::bind(&GlobalPublisherCpp::string_timer_callback, this));

      string_service = this->create_service<ros2_subscriber_publisher::srv::String>("string_srv_cpp", std::bind(&GlobalPublisherCpp::server_string, this, _1, _2)  );
    }

    void string_param_respond()
    {
      this->get_parameter("string_message", new_message);
      if (new_message != message_chosen)
      {
        message_chosen = new_message;
        count_ = 0;
        startUp = true;
      }
    }

  private:


  void server_string(const std::shared_ptr<ros2_subscriber_publisher::srv::String::Request> request,
          std::shared_ptr<ros2_subscriber_publisher::srv::String::Response>      response)
        {
          std::string answer;
          new_message = request->data;
          if (new_message != message_chosen)
          {
            message_chosen = new_message;
            count_ = 0;
            answer = "Got new data!";         
          }
          else
          {
            answer = "No new data, not changing";
          }
          RCLCPP_INFO(this->get_logger(), answer);          
          response->message = answer;
          response->success = true;
        }

    void string_timer_callback()
    {
      if (startUp)
      {
        auto message = std_msgs::msg::String();
        message.data = message_chosen + " " + std::to_string(count_++);
        RCLCPP_INFO(this->get_logger(), "Publishing: '%s'", message.data.c_str());
        string_publisher_->publish(message);
      }

    }
    std::string message_chosen;
    std::string new_message;
    rclcpp::TimerBase::SharedPtr string_timer_;
    rclcpp::TimerBase::SharedPtr param_string_timer_;    
    rclcpp::Publisher<std_msgs::msg::String>::SharedPtr string_publisher_;

    rclcpp::Service<ros2_subscriber_publisher::srv::String>::SharedPtr string_service;
    size_t count_;
    bool startUp;
};

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<GlobalPublisherCpp>());
  rclcpp::shutdown();
  return 0;
}